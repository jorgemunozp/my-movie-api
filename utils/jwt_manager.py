from jwt import encode, decode

def create_token(data: dict):
    token = encode(payload=data, key="apikey", algorithm="HS256")
    return token

def validate_token(token: str) -> dict:
    data: dict = decode(token, key="apikey", algorithms=['HS256'])
    return data
