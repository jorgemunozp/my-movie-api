from fastapi import APIRouter, Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middleware.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


@movie_router.get('/movies', tags=['movies'], response_model=List[Movie],dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(content=jsonable_encoder(result)) # type: ignore


@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie)
def search_movie_by_id(id: int = Path(ge=1, le=2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No movie with that id"}) # type: ignore
    return JSONResponse(status_code=200, content=jsonable_encoder(result)) # type: ignore


@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie])
def get_movies_by_query(category: str = Query(min_length=3, max_length=20)) -> List[Movie]:
    db = Session()
    result = MovieService(db).query_movie_by_category(category)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No movies on that category"}) # type: ignore
    return JSONResponse(status_code=200, content=jsonable_encoder(result)) # type: ignore


@movie_router.post('/movies', tags=['movies'], response_model=dict)
def create_movie(movie: Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(content={"message": "Movie created on the list!"}) # type: ignore

@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict)
def update_movie(id: int, movie:Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No movies with that id"}) # type: ignore
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={"message": "Movie updated!"}) # type: ignore

@movie_router.delete('/movies/{id}', tags=['movies'], response_model=dict)
def delete_movie(id: int) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No movies with that id"}) # type: ignore
    MovieService(db).delete_movie(id)
    return JSONResponse(content={"message": "Movie deleted!"}) # type: ignore
