from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from utils.jwt_manager import create_token
from config.database import engine, Base
from middleware.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.auth import auth_router

app = FastAPI()
app.title = "My Movie FastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(auth_router)

Base.metadata.create_all(bind=engine)

#    class Config:
#        schema_extra = {
#            "example": {
#                "id": 1,
#                "title": "Movie Title",
#                "overview": "Default Overview",
#               "year": 2023,
#               "rating": 5.0,
#                "category": "Default Category"
#            }
#        }

#movies = [
#    {
#        'id': 1,
#        'title': 'Avatar',
#        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
#        'year': '2009',
#        'rating': 7.8,
#        'category': 'Accion'    
#    },
#    {
#        'id': 2,
#        'title': 'Barbie',
#        'overview': "Una version live action de la legendaria muñeca de mattel",
#        'year': '2023',
#        'rating': 7.5,
#        'category': 'Live Action'    
#    }, 
#    {
#        'id': 3,
#        'title': 'Super Mario',
#        'overview': "Una version animada del legendario fontanero y su hermano",
#        'year': '2023',
#        'rating': 8.4,
#        'category': 'Accion'    
#    },
#]



@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>My first FastAPI</h1>')
